%define soname liblibertine
%global debug_package %{nil}


Name:		libertine
Version:	r465
Release:	1%{?dist}
License:	GPL-v3
Summary:	A sandbox for running legacy deb-based X11 applications
URL:		https://github.com/ubports/libertine
Source0:	https://github.com/abhishek9650/libertine/archive/%{name}-%{version}.tar.gz
BuildRequires:	cmake, cmake-extras, dbus-test-runner, intltool, gtest-devel, python3-devel, qt5-qtbase-devel, qt5-qtquickcontrols, qt5-qtdeclarative-devel
BuildRequires:	gtest-devel
Requires:	python3, gtest, qt5-qtbase, qt5-qtquickcontrols
##Rest of the stuff is detected by pkgconfig
BuildRequires:	pkgconfig(glib-2.0)
BuildRequires:	pkgconfig(dbustest-1)
BuildRequires:	pkgconfig(gio-2.0)
BuildRequires:	pkgconfig(gio-unix-2.0)
BuildRequires:	pkgconfig(libsystemd)

%description
A sandbox for running legacy deb-based X11 applications.


%package -n %{soname}
Summary:	A sandbox for running legacy deb-based X11 applications
Group:		System/Libraries

%description -n %{soname}
A sandbox for running legacy deb-based X11 applications.
This package contains libraries to run %{name}


%package devel
Summary:	A sandbox for running legacy deb-based X11 applications
Group:		Development/Libraries
Requires:	%{name} = %{version}
Requires:	%{soname} = %{version}

%description devel
A sandbox for running legacy deb-based X11 applications
This package contains Development files for the package %{name}


%package -n python3-%{name}
Summary:	A sandbox for running legacy deb-based X11 applications
Group:		Python3/Libraries
Requires:	python3-devel

%description -n python3-%{name}
A sandbox for running legacy deb-based X11 applications.
This package contains Python3 site packages for %{name}


%prep
%setup -q
truncate -s 0 tests/CMakeLists.txt


%build
cmake  \
	-DCMAKE_INSTALL_PREFIX:PATH=/usr  \
    -DCMAKE_INSTALL_LIBDIR=%{_libdir}
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot} %{?_smpflags}

%find_lang %{name}

%clean
rm -rf $RPM_BUILD_ROOT



%files -f %{name}.lang
%doc COPYING
%{_mandir}/man1/%{name}-*.1.gz
%{_sysconfdir}/sudoers.d/%{name}-lxc-sudo
%{_sysconfdir}/sudoers.d/%{name}-lxd-sudo
%{_bindir}/%{name}-manager-app
%{_bindir}/%{name}-lxc-setup
%{_bindir}/%{name}-shell
%{_bindir}/%{name}-container-manager
%{_bindir}/libertined
%{_bindir}/%{name}-launch
%{_bindir}/%{name}-xmir
%{_bindir}/%{name}-lxd-setup
%{_datadir}/applications/libertine-manager-app.desktop
%{_datadir}/%{name}/qml/common/*.qml
%{_datadir}/%{name}/qml/gui/*.qml
%{_datadir}/%{name}/*.png
%{_datadir}/%{name}/*.conf
%{_datadir}/bash-completion/completions/libertine-shell
%{_datadir}/bash-completion/completions/libertine-container-manager
%{_datadir}/upstart/sessions/libertine-xmir.conf
%{_libdir}/liblibertine.so
/usr/lib/systemd/user/libertine.service
%{_datadir}/dbus-1/services/com.canonical.libertine.Service.service



%files -n %{soname}
%{_libdir}/liblibertine.so.1
%{_libdir}/liblibertine.so.1.0.0

%files -n python3-%{name}
%{python3_sitelib}/libertine/__pycache__/*.pyc
%{python3_sitelib}/libertine/launcher/__pycache__/*.pyc
%{python3_sitelib}/libertine/service/__pycache__/*.pyc
%{python3_sitelib}/libertine/service/tasks/__pycache__/*.pyc
#pyc files execute faster so i have decided to keep them
%{python3_sitelib}/%{name}/*.py
%{python3_sitelib}/%{name}/launcher/*.py
%{python3_sitelib}/%{name}/service/*.py
%{python3_sitelib}/%{name}/service/tasks/*.py


%files devel
%{_libdir}/pkgconfig/libertine.pc
%{_includedir}/%{soname}/%{name}.h




%changelog

